use std::convert::From;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result as FormatterResult;

use bmp::Pixel;

// Very simple 24bit RGB
#[derive(PartialEq, Clone, Copy)]
pub struct Colour(pub u8, pub u8, pub u8);

impl Display for Colour {
    fn fmt(&self, f: &mut Formatter<'_>) -> FormatterResult {
        write!(f, "Colour ({},{},{}) [RGB]", self.0, self.1, self.2)
    }
}

impl From<Pixel> for Colour {
    #[inline]
    fn from(p: Pixel) -> Self {
        Self(p.r, p.g, p.b)
    }
}

impl From<Colour> for Pixel {
    #[inline]
    fn from(p: Colour) -> Self {
        Self::new(p.0, p.1, p.2)
    }
}

const MASK_RED: u32 = 0x00FF_0000;
const MASK_GREEN: u32 = 0x0000_FF00;
const MASK_BLUE: u32 = 0x0000_00FF;

#[allow(clippy::cast_possible_truncation)]
impl From<u32> for Colour {
    #[inline]
    fn from(v: u32) -> Self {
        Self(
            ((v & MASK_RED) >> 16) as u8,
            ((v & MASK_GREEN) >> 8) as u8,
            (v & MASK_BLUE) as u8,
        )
    }
}

impl From<Colour> for u32 {
    #[inline]
    fn from(c: Colour) -> Self {
        Self::from(c.0) << 16 | Self::from(c.1) << 8 | Self::from(c.2)
    }
}

impl PartialEq<Colour> for u32 {
    #[inline]
    fn eq(&self, to: &Colour) -> bool {
        *self == Self::from(*to)
    }
}
