use crate::colour::Colour;
use crate::pixels::PixelHost;
use crate::point::Point;

pub trait Font {
    fn char_width(&self) -> u32;
    fn char_height(&self) -> u32;
    fn plot(&self, to: &mut dyn PixelHost, p: Point, c: Colour, b: Colour, ch: char);
}

const ORDER: [char; 64] = [
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
    'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '.',
    ',', ':', ';', '(', ')', '[', ']', '{', '}', '>', '<', '/', '\\', '\'', '"', '*', '-', '+',
    '_', '!', '?', '|', '^', '=', '%', '█',
];

impl Font for bmp::Image {
    #[inline]
    fn char_width(&self) -> u32 {
        5
    }

    #[inline]
    fn char_height(&self) -> u32 {
        7
    }

    #[inline]
    fn plot(&self, to: &mut dyn PixelHost, p: Point, c: Colour, b: Colour, ch: char) {
        let f = Colour(255, 255, 255);
        let ch: Vec<char> = ch.to_uppercase().collect();
        let ch = ch[0];
        let mut pos = 0;
        for c in ORDER.iter() {
            if *c == ch {
                break;
            }
            pos += 1;
        }
        for xp in 0..5 {
            for yp in 0..7 {
                let p = Point(p.0 + xp, p.1 + yp, p.2);
                if self.get_pixel((pos * 5) + xp as u32, yp as u32) == f.into() {
                    to.put(p, c);
                } else {
                    to.put(p, b);
                }
            }
        }
    }
}

pub trait DrawString {
    fn draw_string(&mut self, p: Point, c: Colour, b: Colour, f: &dyn Font, s: &str);
}

impl<T> DrawString for T
where
    T: PixelHost,
{
    #[inline]
    fn draw_string(&mut self, p: Point, fg: Colour, bg: Colour, f: &dyn Font, text: &str) {
        let w = u32::from(f.char_width());
        for (i, ch) in text.chars().enumerate() {
            #[allow(clippy::cast_possible_truncation)]
            f.plot(self, Point(p.0 + (w * i as u32), p.1, p.2), fg, bg, ch);
        }
    }
}
