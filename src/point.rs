use crate::pixels::Resolution;

/// X, Y, Res (Width, Height)
#[derive(Clone, Debug, Copy)]
pub struct Point(pub u32, pub u32, pub Resolution);

impl Point {
    pub fn offset(&self, x: u32, y: u32) -> Self {
        Self(self.0 + x, self.1 + y, self.2)
    }
}

impl From<Point> for usize {
    #[inline]
    fn from(p: Point) -> Self {
        // (Rows from top * width) + from left
        ((p.1 * (p.2).0) + p.0) as Self
    }
}
