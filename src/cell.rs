use smallvec::{smallvec, SmallVec};
use std::ops::Index;
use std::ops::IndexMut;

use crate::person::Nation;
use crate::person::{Person, Tick};
use crate::pixels::Resolution;
use crate::point::Point;

#[derive(Clone)]
pub enum Cell {
    Land {
        held_by: Option<Nation>,
        people: SmallVec<[Person; Self::OVERPOPULATION + 2]>,
    },
    Sea(Option<Person>),
}

impl Index<Point> for Vec<Cell> {
    type Output = Cell;

    #[inline]
    fn index(&self, p: Point) -> &Cell {
        let i: usize = p.into(); // Avoid recursion
        &self[i]
    }
}

impl IndexMut<Point> for Vec<Cell> {
    #[inline]
    fn index_mut(&mut self, p: Point) -> &mut Cell {
        let i: usize = p.into(); // Avoid recursion
        &mut self[i]
    }
}

impl Default for Cell {
    fn default() -> Self {
        Cell::Land {
            held_by: None,
            people: SmallVec::with_capacity(Self::OVERPOPULATION + 2),
        }
    }
}

impl Cell {
    const OVERPOPULATION: usize = 5;

    #[inline]
    pub fn tick(
        &mut self,
        w: &[bool],
        r: Resolution,
    ) -> SmallVec<[Person; Self::OVERPOPULATION + 2]> {
        match self {
            Cell::Land { people, held_by } => {
                let mut result = SmallVec::with_capacity(Self::OVERPOPULATION + 2);
                while let Some(mut person) = people.pop() {
                    match person.tick(r, w, false) {
                        Tick::Live => result.push(person),
                        Tick::Child(c) => {
                            result.push(person);
                            result.push(c);
                        }
                        Tick::Dead => {}
                    }
                }
                *held_by = None;
                result
            }
            Cell::Sea(person) => {
                let mut res = SmallVec::with_capacity(0);
                if let Some(swimmer) = person {
                    match swimmer.tick(r, w, true) {
                        Tick::Live => res = smallvec![swimmer.clone()],
                        _ => {}
                    }
                }
                *person = None;
                res
            }
        }
    }

    #[inline]
    pub fn move_into(&mut self, person: &mut Person) {
        match self {
            Cell::Land { people, held_by } => {
                if *held_by == None {
                    *held_by = Some(person.nation);
                    people.push(person.clone());
                } else if *held_by == Some(person.nation) {
                    // Either friendly or taken by force
                    if people.len() > Self::OVERPOPULATION {
                        person.hurt(45);
                    }
                    people.push(person.clone());
                } else {
                    for defender in people.iter_mut() {
                        if !person.fight(defender) {
                            // Attacker defeated
                            return;
                        }
                    }
                    // Successful in taking cell
                    if people.len() > Self::OVERPOPULATION {
                        person.hurt(45);
                    }
                    *held_by = Some(person.nation);
                    people.push(person.clone());
                }
            }
            Cell::Sea(current) => {
                if let Some(ref mut occupier) = current {
                    if !person.fight(occupier) {
                        return;
                    }
                }
                *current = Some(person.clone());
            }
        }
    }
}
