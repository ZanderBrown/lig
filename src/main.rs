#![allow(unknown_lints)]
#![deny(clippy::pedantic)]

use smallvec::SmallVec;
use std::time::Instant;

use bmp;

use minifb::{MouseMode, Window, WindowOptions};

mod cell;
mod colour;
mod person;
mod pixels;
mod point;
mod text;

use crate::cell::Cell;
use crate::colour::Colour;
use crate::person::Person;
use crate::pixels::{PixelHost, Resolution};
use crate::point::Point;
use crate::text::{DrawString, Font};

fn bmp_file(p: &str) -> bmp::Image {
    match bmp::open(p) {
        Err(why) => panic!("Can't load backdrop {}", why),
        Ok(back) => back,
    }
}

const PIXEL_COUNT: usize = 554_400;

fn show_mouse_status(
    window: &Window,
    cells: &Vec<Cell>,
    buffer: &mut [u32; PIXEL_COUNT],
    res: Resolution,
    fg: Colour,
    bg: Colour,
    f: &bmp::Image,
) {
    if let Some(mouse) = window.get_mouse_pos(MouseMode::Discard) {
        let (x, y) = mouse;
        #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
        let x = x.round() as u32;
        #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
        let y = y.round().abs() as u32;
        let p = Point(x, y, res);
        let msg = match &cells[p] {
            Cell::Land { people, held_by } if held_by.is_some() => {
                format!("Land [{}] {} People", held_by.unwrap(), people.len())
            }
            Cell::Land { people, .. } => format!("Land {} People", people.len()),
            Cell::Sea(Some(person)) => format!("Sea [{}]", person.nation),
            Cell::Sea(None) => "Sea".into(),
        };
        buffer.draw_string(
            Point(5, res.1 - 5 - f.char_height(), res),
            fg,
            bg,
            f,
            &format!("[{},{}] {} ", mouse.0, mouse.1, msg),
        );
    }
}

fn show_nation_status(
    nations: &[Colour; 20],
    counts: &[usize],
    buffer: &mut [u32; PIXEL_COUNT],
    res: Resolution,
    fg: Colour,
    bg: Colour,
    f: &bmp::Image,
) {
    #[allow(clippy::cast_possible_truncation)]
    for (nnum, nation) in nations.iter().enumerate() {
        let t = res.1 - 50 - (nnum as u32 * (f.char_height() + 4)) + 3;
        buffer.draw_string(
            Point(22, t + 2, res),
            fg,
            bg,
            f,
            &format!("{}: {} ", nnum, counts[nnum as usize]),
        );

        buffer.rect(Point(5, t + 1, res), 15, f.char_height() + 2, fg);
        buffer.square(Point(6, t + 2, res), 13, f.char_height(), *nation);
    }
}

fn tick(
    cells: &mut Vec<Cell>,
    island: &Vec<bool>,
    nations: &[Colour; 20],
    last_pop: usize,
    buffer: &mut [u32; PIXEL_COUNT],
    res: Resolution,
    fg: Colour,
    bg: Colour,
) -> (usize, [usize; 20]) {
    let mut tick: SmallVec<[Person; 32_768]> = SmallVec::with_capacity(last_pop + 100);
    let mut counts = [0; 20];

    for cell in cells.iter_mut() {
        let people = cell.tick(&island, res);
        for person in people {
            counts[person.nation as usize] += 1;
            tick.push(person);
        }
    }

    for mut person in &mut tick {
        let p = Point(person.x, person.y, res);
        cells[p].move_into(&mut person);
    }

    let pop = tick.len();
    let mut idx = 0;

    for cell in cells {
        buffer[idx] = match cell {
            Cell::Land { held_by, .. } => match held_by {
                Some(held_by) => nations[*held_by as usize].into(),
                None => fg.into(),
            },
            Cell::Sea(person) => match person {
                Some(person) => nations[person.nation as usize].into(),
                None => bg.into(),
            },
        };
        idx += 1;
    }

    (pop, counts)
}

fn main() {
    let map = bmp_file("map.bmp");
    let width = map.get_width();
    let height = map.get_height();

    if (width * height) as usize != PIXEL_COUNT {
        panic!("Expected map.bmp to contain 554,400 pixels");
    }

    let mut window = Window::new(
        "lig",
        width as usize,
        height as usize,
        WindowOptions::default(),
    )
    .unwrap_or_else(|e| {
        panic!("{}", e);
    });

    let res = Resolution(width, height);
    let nations = [
        Colour(255, 255, 0),
        Colour(15, 235, 0),
        Colour(10, 157, 0),
        Colour(255, 98, 98),
        Colour(255, 0, 0),
        Colour(165, 162, 0),
        Colour(255, 150, 0),
        Colour(0, 0, 81),
        Colour(113, 113, 255),
        Colour(0, 0, 255),
        Colour(79, 207, 144),
        Colour(129, 110, 255),
        Colour(255, 120, 240),
        Colour(160, 0, 142),
        Colour(255, 0, 227),
        Colour(88, 88, 88),
        Colour(168, 168, 168),
        Colour(198, 46, 46),
        Colour(115, 208, 129),
        Colour(12, 134, 41),
    ];

    let f = bmp_file("font.bmp");
    let fg = Colour(255, 255, 255);
    let bg = Colour(0, 0, 0);

    let mut buffer: [u32; PIXEL_COUNT] = [0; PIXEL_COUNT];

    let (mut cells, island): (Vec<Cell>, Vec<bool>) = map
        .coordinates()
        .map(|(x, y)| {
            if Colour::from(map.get_pixel(x, y)) == fg {
                (Cell::default(), true)
            } else {
                (Cell::Sea(None), false)
            }
        })
        .unzip();

    #[allow(clippy::cast_possible_truncation)]
    for (i, _) in nations.iter().enumerate() {
        let mut p = Person::initial(i as u8, &island, res);
        let pt = Point(p.x, p.y, res);
        cells[pt].move_into(&mut p);
    }

    let mut cycle = 0;
    let mut last_pop = 0;
    while window.is_open() {
        let start = Instant::now();

        let (pop, counts) = tick(
            &mut cells,
            &island,
            &nations,
            last_pop,
            &mut buffer,
            res,
            fg,
            bg,
        );
        last_pop = pop;

        show_nation_status(&nations, &counts, &mut buffer, res, fg, bg, &f);

        buffer.draw_string(
            Point(5, res.1 - 15 - f.char_height(), res),
            fg,
            bg,
            &f,
            &format!("Total: {} Cycle {} ", pop, cycle),
        );
        cycle += 1;

        show_mouse_status(&window, &cells, &mut buffer, res, fg, bg, &f);

        let elapsed = start.elapsed();
        #[allow(clippy::cast_possible_truncation)]
        let nanos = elapsed.as_nanos() as u32;
        let fps = 10_u32.pow(9) / nanos;
        buffer.draw_string(
            Point(5, res.1 - 25 - f.char_height(), res),
            fg,
            bg,
            &f,
            &format!("FPS: {:.*}", 1, fps),
        );

        // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
        window
            .update_with_buffer(&buffer, width as usize, height as usize)
            .unwrap();
    }
}
