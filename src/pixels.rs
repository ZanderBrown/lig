use crate::colour::Colour;
use crate::point::Point;
use crate::PIXEL_COUNT;

/// Width, Height
#[derive(Debug, Clone, Copy)]
pub struct Resolution(pub u32, pub u32);

// Something that stores pixels
pub trait PixelHost {
    fn put(&mut self, p: Point, c: Colour);
    fn get(&self, p: Point) -> Colour;
    fn square(&mut self, p: Point, w: u32, h: u32, c: Colour) {
        for x in 0..w {
            for y in 0..h {
                self.put(p.offset(x, y), c);
            }
        }
    }

    fn rect(&mut self, p: Point, w: u32, h: u32, c: Colour) {
        for y in 0..h {
            self.put(p.offset(0, y), c);
        }
        for x in 0..w {
            self.put(p.offset(x, 0), c);
        }
        for y in 0..h {
            self.put(p.offset(w - 1, y), c);
        }
        for x in 0..w {
            self.put(p.offset(x, h - 1), c);
        }
    }
}

// Let the games begin!
impl PixelHost for [u32; PIXEL_COUNT] {
    #[inline]
    fn put(&mut self, p: Point, c: Colour) {
        let i: usize = p.into();
        self[i] = c.into();
    }

    #[inline]
    fn get(&self, p: Point) -> Colour {
        let i: usize = p.into();
        self[i].into()
    }
}

#[allow(clippy::cast_possible_truncation)]
impl PixelHost for bmp::Image {
    #[inline]
    fn put(&mut self, p: Point, c: Colour) {
        self.set_pixel(p.0 as u32, p.1 as u32, c.into());
    }

    #[inline]
    fn get(&self, p: Point) -> Colour {
        self.get_pixel(p.0 as u32, p.1 as u32).into()
    }
}
