use rand::{thread_rng, Rng};

use crate::pixels::Resolution;
use crate::point::Point;

pub type Nation = u8;

#[derive(Debug, Clone)]
pub struct Person {
    pub x: u32,
    pub y: u32,
    age: u16,
    strength: u16,
    health: u16,
    birth: u16,
    pub nation: Nation,
}

pub enum Tick {
    Child(Person),
    Dead,
    Live,
}

impl Person {
    #[inline]
    pub fn tick(&mut self, r: Resolution, w: &[bool], sea: bool) -> Tick {
        let mut rnd = thread_rng();
        if self.health < 1 {
            return Tick::Dead; // Died
        }
        self.age += 1;
        if sea {
            // Swimming is harder
            self.strength -= 6;
            if rnd.gen_bool(0.25) {
                self.health -= 1;
            }
            if self.strength < 350 {
                self.health = 0;
            }
        } else if self.age < 160 {
            self.strength += 3;
            self.health += 2;
        } else {
            self.strength -= 2;
            self.health -= 1;
            if self.strength > self.age * 6 {
                self.health = 0;
            }
            if self.age > 190 && self.birth > 80 {
                self.birth -= 60;
                self.health += 2;
                if let Some(child) = self.child() {
                    return Tick::Child(child); // Had a child
                }
            }
        }
        if self.strength < 10 {
            self.health = 0;
        }
        if self.health < 1 {
            return Tick::Dead; // Dead
        } else if self.health > 100 {
            self.birth += 2;
        }
        // Slight chance we stay static, swimmers always move
        if rnd.gen_bool(0.75) || sea {
            loop {
                let mut nx = self.x;
                let mut ny = self.y;
                match rnd.gen_range(0..3) {
                    0 => nx -= 1,
                    2 => nx += 1,
                    _ => nx = nx,
                }
                if nx == 0 {
                    nx += r.0 - 1;
                } else if nx >= r.0 {
                    nx -= r.0;
                }
                match rnd.gen_range(0..3) {
                    0 => ny -= 1,
                    2 => ny += 1,
                    _ => ny = ny,
                }
                if ny == 0 {
                    ny += 2;
                    nx = r.0 - nx;
                } else if ny >= r.1 {
                    ny -= 2;
                    nx = r.0 - nx;
                }
                let p = usize::from(Point(nx, ny, r));
                if sea && w[p] {
                    // Reward landfall
                    self.health += rnd.gen_range(15..35);
                }
                // When already in sea we can go wherever, all land is fine
                // possibly start swimming
                if sea || w[p] || (self.strength > 550 && rnd.gen_bool(0.1)) {
                    self.x = nx;
                    self.y = ny;
                    break;
                }
            }
        }
        Tick::Live
    }

    #[inline]
    pub fn initial(nation: Nation, w: &[bool], res: Resolution) -> Self {
        let mut rnd = thread_rng();
        let (x, y) = loop {
            let x = rnd.gen_range(0..res.0);
            let y = rnd.gen_range(0..res.1);
            if w[usize::from(Point(x, y, res))] {
                break (x, y);
            }
        };
        Self {
            x,
            y,
            age: 0,
            strength: 150 + rnd.gen_range(0..11),
            health: 200 + rnd.gen_range(0..11),
            birth: 50,
            nation,
        }
    }

    #[inline]
    fn child(&mut self) -> Option<Self> {
        let mut rnd = thread_rng();
        let base_strength = 9 * (self.strength / 10);
        let base_health = 8 * (self.health / 10);
        if base_strength < 56 || base_health < 21 {
            // Still born child hurts parent
            self.strength -= 5;
            None
        } else {
            // Reward having children
            self.strength += rnd.gen_range(4..8);
            Some(Self {
                x: self.x,
                y: self.y,
                age: 0,
                strength: if rnd.gen_bool(0.52) {
                    base_strength + rnd.gen_range(0..71)
                } else {
                    base_strength - rnd.gen_range(0..56)
                } + 20,
                health: if rnd.gen_bool(0.52) {
                    base_health + rnd.gen_range(0..31)
                } else {
                    base_health - rnd.gen_range(0..21)
                } + 20,
                birth: rnd.gen_range(0..41),
                nation: self.nation,
            })
        }
    }

    #[inline]
    pub fn hurt(&mut self, amount: u16) {
        if self.health > amount {
            self.health -= amount;
        } else {
            self.health = 0;
        }
    }

    #[inline]
    pub fn fight(&mut self, defender: &mut Self) -> bool {
        if self.health < 10 {
            false
        } else if self.strength >= defender.strength {
            let mut rnd = thread_rng();
            // Reward winning a space
            self.strength += rnd.gen_range(4..24);
            true
        } else {
            false
        }
    }
}

impl PartialEq for Person {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.strength == other.strength
    }
}

impl Eq for Person {}
